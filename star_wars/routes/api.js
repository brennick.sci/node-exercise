var express = require('express');
var router = express.Router();

// Middleware
const Middleware = require('../lib/middleware')

// Routes
router.get('/people',
  Middleware.processRequest,
  Middleware.sortResponseBy,
  Middleware.returnJson
);

router.get('/planets', 
  Middleware.processRequest,
  Middleware.sortResponseBy,
  Middleware.appendSubData,
  Middleware.returnJson
);

module.exports = router;
