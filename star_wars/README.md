# Star Wars API Exercise

As discussed some of our email conversations the original link to [https://swapi.co/](https://swapi.co/)
in no longer being maintained. In order to complete the given assignment, I started working with
the following swapi rest api [https://swapi.py4e.com/api](https://swapi.py4e.com/api]) before receiving an
email suggesting the future completion of the assignment using [https://swapi.dev/](https://swapi.dev/)
rest api. 

If this is an issue, please let me know and I'll convert over the existing code to the suggested rest api
[https://swapi.dev/](https://swapi.dev/). The changes should be minimal and require very little refactoring
as all the existing code is built around a "base url" which can be either of the two maintained rest api
mentioned above.

## Modified files from the original Express.js framework

- The `./routes/index.js` file is not used as it was converted over to `./routes/api.js`
- Both routes (`/people` and `/planets`) can be located in `./routes/api.js`
- The routes are handled using what I've called *Middleware*
  - The code for the middleware is located in `./lib/middleware.js`
  - It was coded this way to help with code reuse
  - Both endpoints work using the same middle ware

## Endpoints (routes)

  - Use of an application like ~Postman~ will facilitate assessing the endpoints
  - Both routes can be accessed with `http://localhost:3000/api/people` and `http://localhost:3000/api/planets`
  - **Both** routes accept the following options via the GET query parameters, both are optional
    - sortBy: can be one or more attributes in the people or planet records that you want to sort with
    - asc: is a flag for determing ascending or descending sorting order
    - Example: `http://localhost:3000/api/people?sortBy=mass&asc=true`
  - **ONLY** the planets route accepts these additional GET query paramters
    - subData: can be one or more attributes in the swapi record that contains links to sub data
    - subAttr: is the name of a single sub attribute that is to be extracted and reloaded as the new
      subData information
    - Example: `http://localhost:3000/api/planets?sortBy=name&asc=false&subData=residents&subAttr=name`

## Downloading of the Records

- Due to the nature of the swapi rest api, batch downloading of the data is not available and downloading
  of high volumes of records may timeout. As a result, located at the top of the `./lib/middleware.js` file
  there are two constants `MAX_RECORDS` and `MAX_SUB_RECORDS` that are used to limit the number of both 
  swapi records and swapi sub records. They can be modified to download all of one of the desired records at
  a time.

## Warning/Problems

- Planet records have a tendency to error out if the limit is to high
- It might have something to do with having to download add the individual people links
  making it max out on it download limit...
