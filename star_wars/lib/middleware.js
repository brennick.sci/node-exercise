const Axios = require('axios')
const Lodash = require('lodash')

// Base REST route
const baseUrl = 'https://swapi.py4e.com/api'

// This limits the number of records that can be pulled at once
// 83 people, 61 planets
const MAX_RECORDS = 10
const MAX_SUB_RECORDS = 10

// RegExp
const urlRegExp = /^https:\/\/swapi/

/**
 * Handler for the initial request. This is the main middleware
 * endpoint processing function. It will consume all the available
 * records from a corresponding swapi endpoint.
 *
 * NOTE
 *  - This is a veriy slow and inefficient process and it may timeout
 *    depending on the number of records that are consecutively 
 *    downloaded from the swapi endpoint
 *
 * @param req {Request} express.js request object
 * @param res {Response} express.js response object
 * @param next {Function} express.js "next" callback to trigger subsequent functions
 */
exports.processRequest = async function(req, res, next) {
  const endPoint = req.path
  res.locals[endPoint] = []

  let count = 1 
  let record = {}

  while(validRecord(record)){
    if(count <= MAX_RECORDS){
      const peopleUrl = `${baseUrl}${endPoint}/${count++}`
      record = await fetchData(peopleUrl)
      res.locals[endPoint].push(record)
    } else {
      break;
    }
  }

  next()
}

/**
 * If the records are to be sorted they will be sorted according to the
 * provided "query string attributes". There are two attributes that are
 * currently being handled:
 *  - sortBy: one or more names corresponding to record attributes
 *  - asc: which determines whether or not to sort ascending or descending
 *
 * @param req {Request} express.js request object
 * @param res {Response} express.js response object
 * @param next {Function} express.js "next" callback to trigger subsequent functions
 */
exports.sortResponseBy = function(req, res, next) {
  const endPoint = req.path
  const { sortBy: orderBy, asc = 'true' } = req.query

  const orderByArray = Lodash.isArray(orderBy) ? orderBy : [orderBy]
  const sortedLocals = Lodash.sortBy(res.locals[endPoint], orderByArray)

  res.locals[endPoint] = asc === 'true' ? sortedLocals : sortedLocals.reverse()

  next()
}

/**
 * Downloads and appends any requested subdata that is of interest to the client.
 *
 * NOTE
 *  - This step makes the downloading process even slower as it will 
 *    retrieve one or more records via the initial swapi record attribute
 *    links.
 *  - Caution should be used when using this feature as you could max out 
 *    your swapi rest api download limit if this sub process is repeatedly 
 *    carried out.
 *  - It is suggested to limit the number of sub records that are included
 *    using the MAX_SUB_RECORDS constant.
 *
 * @param req {Request} express.js request object
 * @param res {Response} express.js response object
 * @param next {Function} express.js "next" callback to trigger subsequent functions
 */
exports.appendSubData = async function(req, res, next) {
  const endPoint = req.path
  const swapiRecords = res.locals[endPoint]
  const { subData = [], subAttr = '' } = req.query // names of the attributes to include
  const attrFields = Lodash.isArray(subData) ? subData : [subData]

  const newSwapiRecords = []
  let record = swapiRecords.shift()

  while(record){
    const processedRecord = await processRecord(record, attrFields, subAttr)
    if(processedRecord){
      newSwapiRecords.push(processedRecord)
    }
    record = swapiRecords.shift()
  }

  res.locals[endPoint] = newSwapiRecords

  next()
}

/**
 * Prcesses a single record from swapi and backfills any requested data 
 * that can be obtained from a link listed one of its attributes
 *
 * @param record {Object} simple single swapi object
 * @param fields {Array} list of swapi object attributes that need to be backfilled
 * @param subAttr {String} name of an attribute to be extracted from the downloaded swapi  sub object
 * @return {Object} updated swapi object
 */
async function processRecord(record, fields, subAttr){
  const attrFields = [...fields]
  if(canProcessSubData(record, attrFields)){
    // process each attribute in the record
    let newRecord = {...record}
    let attrField = attrFields.shift()

    while(attrField){
      const response = await processAttrField(record[attrField], subAttr)
      newRecord = {...newRecord, [attrField]: response}
      attrField = attrFields.shift()
    }
    return newRecord
  }
  return record
}

/**
 * Takes a single attribute field and downloads the endpoint data
 *
 * @param attrFieldData {Array} list of urls from which data is downloaded
 * @param subAttr {String} a single attribute name used for extracting subdata from the swapi object
 * @return {Array} list of data globs extracted from the url
 */
async function processAttrField(attrFieldData, subAttr = 'name'){
  const urls = fetchUrls(attrFieldData)

  // get data from each valid url
  const downloadedData = []
  let url = urls.shift()

  while(url){
    const data = await fetchData(url)
    if(!Lodash.isEmpty(data)){
      if(subAttr){
        const subData = data[subAttr] ? data[subAttr] : 'N/A'
        downloadedData.push(subData)
      } else {
        downloadedData.push(data)
      }
    }
    url = urls.shift()
  }

  return downloadedData
}

/**
 * Helper method to help filter out any strings that are not urls.
 * It will also only return the MAX_SUB_RECORDS number of urls to download.
 *
 * @param dataList {Array} list of possible urls
 * @return {Array} list of valid urls
 */
function fetchUrls(dataList){
  if(!Lodash.isArray(dataList)){ return [] }

  const urls = []
  let index = 0
  
  while(index < MAX_SUB_RECORDS && index < dataList.length){
    const url = dataList[index]
    if(urlRegExp.test(url)){
      urls.push(url)
    }
    index++
  }

  return urls
}

/**
 * Helper for determining if any of the subfields can be processed
 *
 * @param record {Object} swapi record
 * @param attrFields {Array} list of subfields or attributes to process in the record
 * @return {Boolean} true for attributes can be processed, false otherwise
 */
function canProcessSubData(record, attrFields){
  if(attrFields && attrFields.length > 0){
    return attrFields.some(attrField => {
      return !Lodash.isEmpty(record[attrField])
    })
  }
  return false
}

/**
 * Helper to repackage the middleware record set that is returned to the client
 *
 * @param req {Request} express.js request object
 * @param res {Response} express.js response object
 */
exports.returnJson = function(req, res) {
  res.json(res.locals[req.path])
}

/**
 * Helper to determine if their are any more records to be downloaded from 
 * the swapi REST api.
 *
 * @param record {Object} simple record object returned from swapi
 * @returns {Boolean} true for more records available, false otherwise
 */
function validRecord(record){
  return record.detail && record.detail === 'Not found' ? false : true
}

/**
 * The main middleware function that handles retrieving single records from swapi
 * in a NON-ASYNC way, so the records can be collected and returned to the client
 *
 * @param url {String} the swapi endpoint to be hit for downloading records
 * @return {Object} a "single" swapi rest api object
 */
async function fetchData(url){
  try {
    const response = await Axios.get(url);
    return response.data
  } catch (error) {
    return ''
  }
}
